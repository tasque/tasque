FROM centurylink/ca-certs
ADD ./build/tasque /usr/bin/
ENTRYPOINT ["/usr/bin/tasque"]
